class Link < ActiveRecord::Base
	validates :url, :presence => true
	validates :url, :url => true
	before_create :randomize_short
	
	belongs_to :user

	private

	def randomize_short
		until !Link.where(short: short).exists?
  		self.short = SecureRandom.urlsafe_base64(4)
		end
	end
	
end
