class Message

  include ActiveModel::Model
  include ActiveModel::Conversion
  include ActiveModel::Validations

  attr_accessor :name, :email, :content

  validates :name,
    presence: true,
    length: { maximum: 200 }

  validates :email,
    presence: true,
    length: { maximum: 200 }

  validates :content,
    presence: true,
    length: { maximum: 5000 }

end