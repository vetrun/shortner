class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  private
 
  def require_admin
    if !current_user.try(:admin?)
    	flash[:notice] = "Link Not Found"
      redirect_to root_path
    end	
  end

end
