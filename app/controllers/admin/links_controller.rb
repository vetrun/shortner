class Admin::LinksController < ApplicationController
	before_action :require_admin

  def index
    	@links = Link.all
  end

end

